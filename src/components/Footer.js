import React from 'react';
import './Footer.css';

function Footer() {
	return (
		<footer>
			<div className="main-wrapper">
				<div className="footer-content">
					<div className="row">
						<div className="col-lg-4 col-md-12">
							<div className="footer-box">
								<div className="footer-contact">
									<span className="icon-box-footer">
										<i className="fab fa-facebook-f"></i>
									</span>
									<span className="icon-box-footer">
										<i className="fab fa-instagram"></i>
									</span>
									<span className="icon-box-footer">
										<i className="fab fa-linkedin-in"></i>
									</span>
								</div>
								<form>
									<div className="form-group">
										<input type="text" className="form-control" />
										<button type="submit" className="btn btn__default">
											search
										</button>
									</div>
								</form>
							</div>
						</div>
						<div className="col-lg-4 col-md-12">
							<div className="footer-box">
								<h4 className="footer-title text-center">Lorem ipsum dolor</h4>
								<p className="footer-text">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
									do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									Ut enim ad commodo consequat.
								</p>
							</div>
						</div>
						<div className="col-lg-4 col-md-12">
							<div className="footer-box">
								<h4 className="footer-title text-center">Lorem ipsum dolor</h4>
								<p className="footer-text">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
									do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									Ut enim ad commodo consequat.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div className="footer-copyright">
					<span className="copyright">
						&copy; Copyright 2021 - Powered by MSM
					</span>
					<img src="/images/logo.png" className="footer-logo" alt="Logo" />
				</div>
			</div>
		</footer>
	);
}

export default Footer;
