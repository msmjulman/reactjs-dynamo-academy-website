import React from 'react';
import Button from './Button';
import './Hero.css';

function Hero({ goToServicesSection }) {
	return (
		<header id="hero">
			<div className="head-wrapper">
				<div className="row">
					<div className="col-md-4">
						<div className="hero-img-container">
							<div className="img-dynamo">
								<img
									src="/images/dynamo.png"
									className="img-fluid flow"
									alt="dynamo"
								/>
							</div>
							<div className="img-shadow">
								<img src="/images/shadow.png" alt="dynamo-shadow" />
							</div>
						</div>
					</div>
					<div className="col-md-8">
						<div className="hero-text-container">
							<h1 className="hero-lead">Upgrade your Skills</h1>
							<p className="hero-slogan">
								Lorem ipsum dolor amet, consectetur adipi sicing elit, sed do
								eiusmod tempor incididunt ut labore magna aliqua anim id
								laborum.
							</p>
							<Button
								buttonPath="fake"
								buttonStyle="btn__important"
								buttonSize="btn__large"
								typeButton={true}
								onClick={goToServicesSection}>
								Get Started
							</Button>
						</div>
					</div>
				</div>
			</div>
		</header>
	);
}

export default Hero;
