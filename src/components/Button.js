import React from 'react';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import './Button.css';

const STYLES = ['btn__primary', 'btn__secondary', 'btn__important'];
const SIZE = ['btn__large', 'btn__medium', 'btn__small'];

function Button({
	buttonStyle,
	buttonSize,
	children,
	buttonPath,
	typeButton,
	onClick,
}) {
	const checkButtonStyle = STYLES.includes(buttonStyle)
		? buttonStyle
		: STYLES[0];
	const checkButtonSize = SIZE.includes(buttonSize) ? buttonSize : SIZE[1];

	const checkTypeButton = typeButton ? true : false;
	if (checkTypeButton) {
		return (
			<button
				className={`btn ${checkButtonStyle} ${checkButtonSize}`}
				onClick={onClick}>
				{children}
			</button>
		);
	} else {
		return (
			<Link
				className={`btn ${checkButtonStyle} ${checkButtonSize}`}
				to={buttonPath}>
				{children}
			</Link>
		);
	}
}

export default Button;
