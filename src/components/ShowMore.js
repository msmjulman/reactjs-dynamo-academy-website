import React from 'react';
import './ShowMore.css';
import Button from './Button';

function ShowMore() {
	return (
		<section id="show-more">
			<div className="main-wrapper">
				<div className="row">
					<div className="col-md-7 order-md-first order-sm-last order-last">
						<div className="show-more-text-container">
							<h2 className="show-more-title">Lorem ipsum dolor sit</h2>
							<p className="show-more-body">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
								eiusmod tempor incididunt ut labore et dolore magna aliqua.
							</p>
							<Button
								buttonPath="/features"
								buttonSize="btn__medium"
								buttonStyle="btn__primary">
								En savoir plus
							</Button>
						</div>
					</div>
					<div className="col-md-5 order-md-last order-sm-first order-first">
						<div className="show-more-img-container">
							<img src="/images/6.jpg" className="img-fluid" alt="Girl Algo" />
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}

export default ShowMore;
