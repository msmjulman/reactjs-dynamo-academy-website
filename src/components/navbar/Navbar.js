import React, { Component } from 'react';
import './Navbar.css';
import Button from '../Button';
import { menuItems } from './menuItems';
import { NavLink, Link } from 'react-router-dom/cjs/react-router-dom.min';

class Navbar extends Component {
	state = { clicked: false };

	handleClick = () => {
		this.setState({ clicked: !this.state.clicked });
	};
	render() {
		return (
			<nav className="navbar navbar-expand fixed-top">
				<div className="navbar-content head-wrapper">
					<Link to="/" className="logo-container">
						<img src="/images/logo-white.png" alt="logo" />
					</Link>
					<div className="menu-icon" onClick={this.handleClick}>
						<i
							className={
								this.state.clicked ? 'fas fa-times' : 'fas fa-bars'
							}></i>
					</div>
					<ul
						className={`nav navbar-nav ${
							this.state.clicked ? 'active' : ''
						} ml-auto`}>
						{menuItems.map((item, key) => (
							<li className={`nav-item`} key={key} onClick={this.handleClick}>
								<NavLink className={item.class_name} exact to={item.url}>
									{item.title}
								</NavLink>
							</li>
						))}
					</ul>
					<Button
						buttonPath="/sign-in"
						buttonStyle="btn__secondary"
						buttonSize="btn__small">
						Sign In
					</Button>
				</div>
			</nav>
		);
	}
}

export default Navbar;
