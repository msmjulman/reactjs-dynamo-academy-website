export const menuItems = [
	{ title: 'Home', url: '/', class_name: 'nav-link' },
	{ title: 'Features', url: '/features', class_name: 'nav-link' },
	{ title: 'About', url: '/about', class_name: 'nav-link' },
	{ title: 'Blog', url: '/blog', class_name: 'nav-link' },
	{ title: 'Sign In', url: '/sign-in', class_name: 'nav-link-mobile' },
];
