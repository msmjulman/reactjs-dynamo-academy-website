import React from 'react';
import BandCard from './BandCard';
import { bandItems } from './bandItems';
import './Band.css';

function Band() {
	return (
		<div id="band">
			<div className="main-wrapper">
				<div className="row">
					{bandItems.map((item, key) => (
						<div className="col-md-4" key={key}>
							<BandCard title={item.title} icon={item.bandIcon} />
						</div>
					))}
				</div>
			</div>
		</div>
	);
}

export default Band;
