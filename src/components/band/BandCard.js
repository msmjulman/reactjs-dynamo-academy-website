import React from 'react';
import './BandCard.css';

function BandCard({ title, icon }) {
	return (
		<div className="band-card">
			<span className="band-icon">
				<i className={icon}></i>
			</span>
			<p className="band-title mb-0">{title}</p>
		</div>
	);
}

export default BandCard;
