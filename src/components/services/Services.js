import React from 'react';
import ServiceCard from './ServiceCard';
import './Services.css';
import { serviceItems } from './servicesItems';

function Services({ servicesSection }) {
	return (
		<section id="services" ref={servicesSection}>
			<div className="main-wrapper">
				<div className="services-container">
					<h1 className="main-title">Services</h1>
					<div className="services-container">
						<div className="row">
							{serviceItems.map((item, key) => (
								<div className="col-md-6" key={key}>
									<ServiceCard
										title={item.title}
										body={item.body}
										icon={item.serviceIcon}
									/>
								</div>
							))}
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}

export default Services;
