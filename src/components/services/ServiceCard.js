import React from 'react';
import './ServiceCard.css';

function ServiceCard({ title, body, icon }) {
	return (
		<div className="service-card">
			<div className="icon-container">
				<span className="icon-box">
					<i className={icon}></i>
				</span>
			</div>
			<div className="text-container">
				<h4 className="service-title">{title}</h4>
				<p className="service-description">{body}</p>
			</div>
		</div>
	);
}

export default ServiceCard;
