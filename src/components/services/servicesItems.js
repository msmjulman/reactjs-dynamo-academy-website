export const serviceItems = [
	{
		title: 'Ipsum Dolor elit',
		body:
			'Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
		serviceIcon: 'fas fa-paper-plane',
	},
	{
		title: 'Ullamco laboris',
		body:
			'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo',
		serviceIcon: 'fas fa-lightbulb',
	},
	{
		title: 'Essecillum dolore',
		body:
			'Voluptate velit essecillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non.',
		serviceIcon: 'fas fa-code',
	},
	{
		title: 'tempor incididunt',
		body:
			'Adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua consectetur.',
		serviceIcon: 'fas fa-database',
	},
	{
		title: 'Officia deserunt',
		body:
			'Sunt in culpa qui officia deserunt mollit anim id est laborum enim ad veniam, quis abore.',
		serviceIcon: 'fas fa-bug',
	},
	{
		title: 'Consectetur adip',
		body:
			'Mollit anim est laborum enim minim veniam consectetur adipisicing elit, sed do eiusmod.',
		serviceIcon: 'fas fa-code-branch',
	},
];
