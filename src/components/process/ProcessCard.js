import React from 'react';
import './ProcessCard.css';

function ProcessCard({ image, title, body }) {
	return (
		<div className="process-card">
			<div className="process-img-container">
				<img src={image} className="img-fluids" alt="Process-Img" />
			</div>
			<div className="process-text-container">
				<h3 className="process-title">{title}</h3>
				<p className="process-body mb-0">{body}</p>
			</div>
		</div>
	);
}

export default ProcessCard;
