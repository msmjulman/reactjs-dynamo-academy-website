export const processItems = [
	{
		imgUrl: '/images/1.jpg',
		title: 'Lorem ipsum dolor',
		body:
			'Lorem ipsum dolor sit amet, sed consectetur adipisicing elit, do eiusmod nostrud exercitation',
	},
	{
		imgUrl: '/images/2.jpg',
		title: 'Lorem ipsum dolor',
		body:
			'Lorem ipsum dolor sit amet, sed consectetur adipisicing elit, do eiusmod nostrud exercitation',
	},
	{
		imgUrl: '/images/3.jpg',
		title: 'Lorem ipsum dolor',
		body:
			'Lorem ipsum dolor sit amet, sed consectetur adipisicing elit, do eiusmod nostrud exercitation',
	},
	{
		imgUrl: '/images/4.jpg',
		title: 'Lorem ipsum dolor',
		body:
			'Lorem ipsum dolor sit amet, sed consectetur adipisicing elit, do eiusmod nostrud exercitation',
	},
	{
		imgUrl: '/images/5.jpg',
		title: 'Lorem ipsum dolor',
		body:
			'Lorem ipsum dolor sit amet, sed consectetur adipisicing elit, do eiusmod nostrud exercitation',
	},
];
