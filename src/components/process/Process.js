import React from 'react';
import ProcessCard from './ProcessCard';
import { processItems } from './processItems';
import './Process.css';

function Process() {
	return (
		<section id="process">
			<div className="main-wrapper">
				<div className="process-container">
					<h1 className="main-title">process</h1>
					<div className="process-container">
						<div className="row">
							{processItems.map((item, key) => (
								<div
									className={
										key > processItems.length - 3
											? 'col-lg-6 col-md-6'
											: 'col-lg-4 col-md-6'
									}
									key={key}>
									<ProcessCard
										image={item.imgUrl}
										title={item.title}
										body={item.body}
									/>
								</div>
							))}
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}

export default Process;
