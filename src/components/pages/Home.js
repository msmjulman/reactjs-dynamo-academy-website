import React, { useRef } from 'react';
import Band from '../band/Band';
import Hero from '../Hero';
import Process from '../process/Process';
import Services from '../services/Services';
import ShowMore from '../ShowMore';

function Home() {
	const servicesSection = useRef(null);
	const goToServicesSection = () => {
		window.scrollTo({
			top: servicesSection.current.offsetTop,
			behavior: 'smooth',
		});
	};
	return (
		<>
			<Hero goToServicesSection={goToServicesSection} />
			<Services servicesSection={servicesSection} />
			<Process />
			<Band />
			<ShowMore />
		</>
	);
}

export default Home;
