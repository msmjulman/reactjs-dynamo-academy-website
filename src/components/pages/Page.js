import React from 'react';
import Button from '../Button';
import './Page.css';

function Page({ title }) {
	return (
		<>
			<div className="head">
				<div className="head-overlay">
					<h1 className="head-title">{title}</h1>
				</div>
			</div>
			<div className="main-wrapper">
				<div className="page-content">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
						eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
						ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
						aliquip ex ea commodo consequat. Duis aute irure dolor in
						reprehenderit in voluptate velit essecillum dolore eu fugiat nulla
						pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
						culpa qui officia deserunt mollit anim id est laborum enim ad minim
						veniam, quis abore et dolore voluptate
					</p>
					<Button
						buttonPath="/"
						buttonStyle="btn__primary"
						buttonSize="btn__medium">
						Back to Home
					</Button>
				</div>
			</div>
		</>
	);
}

export default Page;
