import React from 'react';
import Page from './Page';

function Blog() {
	return (
		<section id="blog">
			<Page title="Blog" />
		</section>
	);
}

export default Blog;
