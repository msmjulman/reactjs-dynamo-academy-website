import React from 'react';
import Page from './Page';

function About() {
	return (
		<section id="about">
			<Page title="About" />
		</section>
	);
}

export default About;
