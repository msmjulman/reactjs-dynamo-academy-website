import React from 'react';
import Page from './Page';

function Features() {
	return (
		<section id="features">
			<Page title="Features" />
		</section>
	);
}

export default Features;
