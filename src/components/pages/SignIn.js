import React from 'react';
import './SignIn.css';

function SignIn() {
	return (
		<section id="sign-in">
			<div className="main-wrapper">
				<div className="sign-in-container">
					<div className="row">
						<div className="col-md-7">
							<div className="sign-in-img ">
								<img
									src="/images/10.png"
									className="img-fluid "
									alt="Sign In"
								/>
							</div>
						</div>
						<div className="col-md-5">
							<div className="sign-in-form ">
								<h2 className="sign-title ">Connexion</h2>
								<form>
									<div className="form-group">
										<label htmlFor="username">Username</label>
										<input
											type="text"
											className="form-control"
											name="username"
											id="username"
										/>
									</div>
									<div className="form-group">
										<label htmlFor="password">Password</label>
										<input
											type="text"
											className="form-control"
											name="password"
											id="password"
										/>
									</div>
									<button type="submit" className="btn btn-primary btn-block">
										Connexion
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}

export default SignIn;
