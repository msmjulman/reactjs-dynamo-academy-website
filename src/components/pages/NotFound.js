import React from 'react';

function NotFound() {
	return (
		<section id="not-found">
			<div className="img-404-container">
				<div className="not-found-overlay">
					<div className="main-wrapper">
						<h2 className="text-center">404 Page Not Found...</h2>
					</div>
				</div>
			</div>
		</section>
	);
}

export default NotFound;
