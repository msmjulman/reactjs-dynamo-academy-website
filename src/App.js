import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import Footer from './components/Footer';
import Navbar from './components/navbar/Navbar';
import About from './components/pages/About';
import Blog from './components/pages/Blog';
import Features from './components/pages/Features';
import Home from './components/pages/Home';
import NotFound from './components/pages/NotFound';
import SignIn from './components/pages/SignIn';
import ScrollToTop from './components/ScrollToTop';
import Services from './components/services/Services';

class App extends Component {
	render() {
		return (
			<div className="App">
				<Router>
					<ScrollToTop />
					<Navbar />
					<Switch>
						<Route path="/" exact component={Home} />
						<Route path="/services" exact component={Services} />
						<Route path="/features" exact component={Features} />
						<Route path="/about" exact component={About} />
						<Route path="/blog" exact component={Blog} />
						<Route path="/sign-in" exact component={SignIn} />
						<Route component={NotFound} />
					</Switch>
					<Footer />
				</Router>
			</div>
		);
	}
}
export default App;
